package com.example.appforum.component;

import com.example.appforum.entity.Post;
import com.example.appforum.entity.Role;
import com.example.appforum.entity.Tag;
import com.example.appforum.entity.User;
import com.example.appforum.enums.RoleName;
import com.example.appforum.repository.PostRepository;
import com.example.appforum.repository.RoleRepository;
import com.example.appforum.repository.TagRepository;
import com.example.appforum.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
//@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TagRepository tagRepository;



    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {

            Role user = roleRepository.save(new Role(10, RoleName.ROLE_USER));
            Role admin = roleRepository.save(new Role(20, RoleName.ROLE_ADMIN));
            List<Tag> tagList = Arrays.asList
                    (new Tag(10, "java"), new Tag(20, "spring"), new Tag(30, "sql"),
                            new Tag(40, "json"), new Tag(50, "oracle"), new Tag(60, "android"));
            tagRepository.saveAll(tagList);


            userRepository.save(
                    new User(
                            "admin",
                            "user",
                            "qwerrty@mail.ru",
                            passwordEncoder.encode("12345"),
                            roleRepository.findAll()

                    )
            );
        }
    }
}

