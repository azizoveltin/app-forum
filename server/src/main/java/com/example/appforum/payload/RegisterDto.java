package com.example.appforum.payload;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class RegisterDto {

    @Email(message = "It should have email format")
    @NotBlank(message = "User email is required")
    private String email;

    @NotEmpty(message = "Please enter your firstname")
    private String firstName;

    @NotEmpty(message = "Please enter your lastname")
    private String lastName;

    @NotEmpty(message = "Password is required")
    @Size(min = 8)
    private String password;

}
