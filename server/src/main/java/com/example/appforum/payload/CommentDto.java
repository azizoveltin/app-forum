package com.example.appforum.payload;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

@Data
public class CommentDto {
    @NotEmpty
    private String answer;
}
