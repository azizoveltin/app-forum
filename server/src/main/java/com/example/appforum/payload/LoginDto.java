package com.example.appforum.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginDto {
    @Email
    @NotEmpty(message = "enter email")
    private String email;
    @NotEmpty(message = "enter password")
    private String password;
}
