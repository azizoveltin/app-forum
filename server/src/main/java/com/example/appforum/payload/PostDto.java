package com.example.appforum.payload;

import com.example.appforum.entity.Tag;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Set;

@Data
public class PostDto {
    @NotEmpty
    private String title;
    @NotEmpty
    private String body;
    private Integer vote;
    private List<Integer> tags;
    private List<String> voteUser;
}
