package com.example.appforum.security;


import com.example.appforum.entity.Role;
import com.example.appforum.entity.User;
import com.example.appforum.enums.RoleName;
import com.example.appforum.payload.ApiResponse;
import com.example.appforum.payload.RegisterDto;
import com.example.appforum.repository.RoleRepository;
import com.example.appforum.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    public ApiResponse addUser(RegisterDto reqUser) {

        if (!userRepository.existsByEmail(reqUser.getEmail())) {
            User user = new User();
            user.setFirstName(reqUser.getFirstName());
            user.setLastName(reqUser.getLastName());
            user.setEmail(reqUser.getEmail());
            user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
            user.setRoles(Collections.singletonList(roleRepository.findByName(RoleName.ROLE_USER)));
            userRepository.save(user);
            return new ApiResponse("User ro'yxatdan o'tkazildi", true);
        }
        return new ApiResponse("Bunday email  user sistemada mavjud", false);
    }


    public ApiResponse getUserById(UUID id) {

        userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("not found"));

        return new ApiResponse("success", true);
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findUserByEmail(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }


}
