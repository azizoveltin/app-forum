package com.example.appforum.service;

import com.example.appforum.entity.Comment;
import com.example.appforum.entity.Post;
import com.example.appforum.entity.User;
import com.example.appforum.exception.PostNotFoundException;
import com.example.appforum.facade.CommentFacade;
import com.example.appforum.facade.PostFacade;
import com.example.appforum.facade.dto.ResCommentDto;
import com.example.appforum.facade.dto.ResPostDto;
import com.example.appforum.payload.ApiResponse;
import com.example.appforum.payload.CommentDto;
import com.example.appforum.repository.CommentRepository;
import com.example.appforum.repository.PostRepository;
import com.example.appforum.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentFacade commentFacade;


    public ResCommentDto addComment(Integer postId, CommentDto commentDto, User user) {

        Post post = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException("post not found"));
        Comment comment = new Comment();
        comment.setPost(post);
        comment.setAnswer(commentDto.getAnswer());
        comment.setEmail(user.getEmail());
        commentRepository.save(comment);
        ResCommentDto resCommentDto = commentFacade.commentToCommentDTO(comment);

        return resCommentDto;
    }

    public List<Comment> getAllCommentForPost(Integer id) {

        Post post = postRepository.findById(id).orElseThrow(() -> new PostNotFoundException("post not found"));

        return commentRepository.findAllByPost(post);
    }

    public ApiResponse deleteComment(Integer id) {

        try {
            commentRepository.deleteById(id);
            return new ApiResponse("success", true);
        } catch (Exception e) {
            return new ApiResponse("not found", false);
        }
    }
}

