package com.example.appforum.service;


import com.example.appforum.entity.Post;
import com.example.appforum.entity.Tag;
import com.example.appforum.entity.User;
import com.example.appforum.exception.PostNotFoundException;
import com.example.appforum.facade.PostFacade;
import com.example.appforum.facade.dto.ResPostDto;
import com.example.appforum.payload.ApiResponse;
import com.example.appforum.payload.PostDto;
import com.example.appforum.repository.PostRepository;
import com.example.appforum.repository.TagRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final TagRepository tagRepository;
    private final PostFacade postFacade;

    public ApiResponse addPost(PostDto postDto, User user) {

        Post post = new Post();
        post.setUser(user);
        post.setTitle(postDto.getTitle());
        post.setBody(postDto.getBody());
        post.setVote(0);
        List<Tag> tagList = tagRepository.findAllById(postDto.getTags());
        post.setTags(tagList);
        postRepository.save(post);
        ResPostDto resPostDto = postFacade.postToPostDTO(post);
        return new ApiResponse("success", true, resPostDto);
    }


    public List<Post> getAllPosts() {

        return postRepository.findAll();
    }

    public List<Post> getAllPostsByUser(User user) {

        return postRepository.findAllByUser(user);

    }

    public ApiResponse getPostById(Integer id, User user) {

        postRepository.findByIdAndUser(id, user).orElseThrow(() -> new PostNotFoundException("post not found"));

        return new ApiResponse("success", true);
    }

    public ApiResponse votePost(Integer postId, String email) {

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Post cannot be found"));

        Optional<String> userVoted = post.getVoteUser()
                .stream()
                .filter(u -> u.equals(email)).findAny();

        if (userVoted.isPresent()) {
            post.setVote(post.getVote() - 1);
            post.getVoteUser().remove(email);
        } else {
            post.setVote(post.getVote() + 1);
            post.getVoteUser().add(email);
        }
        postRepository.save(post);

        return new ApiResponse("success", true);

    }

    public ApiResponse deletepost(Integer id) {
        try {
            postRepository.deleteById(id);

            return new ApiResponse("success", true);
        } catch (Exception e) {
            return new ApiResponse("not found", false);
        }
    }


}
