package com.example.appforum.repository;


import com.example.appforum.entity.Role;
import com.example.appforum.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {

    Role findByName(RoleName roleUser);
}
