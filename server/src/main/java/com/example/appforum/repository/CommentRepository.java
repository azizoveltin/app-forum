package com.example.appforum.repository;

import com.example.appforum.entity.Comment;
import com.example.appforum.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findAllByPost(Post post);

    Comment findByIdAndCreatedBy(Integer id, UUID userId);
}
