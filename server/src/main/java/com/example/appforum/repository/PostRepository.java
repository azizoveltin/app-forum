package com.example.appforum.repository;



import com.example.appforum.entity.Post;
import com.example.appforum.entity.Tag;
import com.example.appforum.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

    List<Post> findAllByUser(User user);
    Optional<Post> findByIdAndUser(Integer id, User user);
}
