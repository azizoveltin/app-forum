package com.example.appforum.controller;


import com.example.appforum.entity.User;
import com.example.appforum.facade.UserFacade;
import com.example.appforum.facade.dto.ResUserDto;
import com.example.appforum.payload.ApiResponse;
import com.example.appforum.payload.JwtResponse;
import com.example.appforum.payload.LoginDto;
import com.example.appforum.payload.RegisterDto;
import com.example.appforum.repository.UserRepository;
import com.example.appforum.security.AuthService;
import com.example.appforum.security.CurrentUser;
import com.example.appforum.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;


@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final UserFacade userFacade;

    public static final String TOKEN_PREFIX = "Bearer ";


    @PostMapping("/register")
    public HttpEntity<?> signUp(@Valid @RequestBody RegisterDto reqUser) {
        ApiResponse response = authService.addUser(reqUser);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @PostMapping("/login")
    public HttpEntity<?> signIn(@Valid @RequestBody LoginDto reqLogin) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        reqLogin.getEmail(),
                        reqLogin.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = TOKEN_PREFIX + jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(true,token));

    }

    @GetMapping("/me")
    public HttpEntity<?> getCurrentUser(@AuthenticationPrincipal UserDetails user) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user1 = (User) authentication.getPrincipal();
        ResUserDto resUserDto = userFacade.userToUserDTO(user1);
        return ResponseEntity.ok(resUserDto);
    }


}
