package com.example.appforum.controller;

import com.example.appforum.entity.Comment;
import com.example.appforum.entity.User;
import com.example.appforum.facade.CommentFacade;
import com.example.appforum.facade.dto.ResCommentDto;
import com.example.appforum.payload.ApiResponse;
import com.example.appforum.payload.CommentDto;
import com.example.appforum.security.CurrentUser;
import com.example.appforum.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentFacade commentFacade;

    @PostMapping("/{postId}/create")
    public HttpEntity<?> addComment(@Valid @RequestBody CommentDto commentDto, @PathVariable Integer postId, @CurrentUser User user) {

        ResCommentDto resCommentDto = commentService.addComment(postId, commentDto, user);
        return new ResponseEntity<>(resCommentDto, HttpStatus.OK);

    }

    @GetMapping("/{postId}/all")
    public HttpEntity<?> getAllCommentsToPost(@PathVariable Integer postId) {

        List<ResCommentDto> commentDtoList = commentService.getAllCommentForPost(postId)
                .stream()
                .map(commentFacade::commentToCommentDTO)
                .collect(Collectors.toList());

        return new ResponseEntity<>(commentDtoList, HttpStatus.OK);

    }

    @DeleteMapping("/{commentId}/delete")
    public HttpEntity<?> deleteComment(@PathVariable Integer commentId) {

        ApiResponse response = commentService.deleteComment(commentId);

        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(response);
    }

}
