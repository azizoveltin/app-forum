package com.example.appforum.controller;

import com.example.appforum.entity.Post;
import com.example.appforum.entity.Tag;
import com.example.appforum.exception.PostNotFoundException;
import com.example.appforum.facade.TagFacade;
import com.example.appforum.repository.PostRepository;
import com.example.appforum.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/tag")
public class TagController {

    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private TagFacade tagFacade;

    @GetMapping("/all")
    public HttpEntity<?> getAll() {
        List<Tag> tagList = tagRepository.findAll()
                .stream()
                .map(tagFacade::resTag)
                .collect(Collectors.toList());
        return new ResponseEntity<>(tagList, HttpStatus.OK);
    }

    @GetMapping("/{postId}/all")
    public HttpEntity<?> getAllTagsToPost(@PathVariable Integer postId){

        Post post = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException("post not found"));

//        return commentRepository.findAllByPost(post);

        return null;
    }
}
