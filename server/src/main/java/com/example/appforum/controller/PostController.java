package com.example.appforum.controller;

import com.example.appforum.entity.User;
import com.example.appforum.facade.PostFacade;
import com.example.appforum.facade.UserFacade;
import com.example.appforum.facade.dto.ResPostDto;
import com.example.appforum.facade.dto.ResUserDto;
import com.example.appforum.payload.ApiResponse;
import com.example.appforum.payload.PostDto;
import com.example.appforum.security.CurrentUser;
import com.example.appforum.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/post")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;
    private final PostFacade postFacade;
    private final UserFacade userFacade;

    @PostMapping
//    @PreAuthorize("hasRole('ROLE_ADMIN')") true
    public HttpEntity<?> addPost(@Valid @RequestBody PostDto postDto, @CurrentUser User user) {
        ApiResponse response = postService.addPost(postDto, user);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    //    @PreAuthorize("hasAuthority('ROLE_ADMIN')")true
    @GetMapping("/all")
    public HttpEntity<?> getAllPosts() {
        List<ResPostDto> postDtoList = postService.getAllPosts()
                .stream()
                .map(postFacade::postToPostDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(postDtoList, HttpStatus.OK);
    }

    @GetMapping("/user/posts")
    public HttpEntity<?> getAllPostsForUser(@CurrentUser User user) {
        List<ResPostDto> postDtoList = postService.getAllPostsByUser(user)
                .stream()
                .map(postFacade::postToPostDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(postDtoList, HttpStatus.OK);
    }

    @PostMapping("/{postId}/{email}/vote")
    public HttpEntity<?> votePost(@PathVariable Integer postId, @PathVariable String email) {

        ApiResponse response = postService.votePost(postId, email);

        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("/{id}/delete")
    public HttpEntity<?> deletePost(@PathVariable Integer id) {
        ApiResponse response = postService.deletepost(id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(response);
    }

    @GetMapping("/user")
    public HttpEntity<?> getUser(@CurrentUser User user) {

        ResUserDto resUserDto = userFacade.userToUserDTO(user);

        return new ResponseEntity<>(resUserDto, HttpStatus.OK);
    }

}
