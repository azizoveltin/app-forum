package com.example.appforum.facade;

import com.example.appforum.entity.Comment;
import com.example.appforum.facade.dto.ResCommentDto;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class CommentFacade {
    public ResCommentDto commentToCommentDTO(Comment comment) {
        ResCommentDto resCommentDto = new ResCommentDto();
        resCommentDto.setId(comment.getId());
        resCommentDto.setEmail(comment.getEmail());
        resCommentDto.setAnswer(comment.getAnswer());
        return resCommentDto;
    }
}
