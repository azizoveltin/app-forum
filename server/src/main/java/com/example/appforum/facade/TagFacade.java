package com.example.appforum.facade;

import com.example.appforum.entity.Post;
import com.example.appforum.entity.Tag;
import com.example.appforum.facade.dto.ResPostDto;
import com.example.appforum.security.CurrentUser;
import org.springframework.stereotype.Component;

@Component
public class TagFacade {

    public Tag resTag(Tag tag) {

        tag.setId(tag.getId());
        tag.setName(tag.getName());
        return tag;

    }
}
