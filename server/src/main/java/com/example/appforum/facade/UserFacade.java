package com.example.appforum.facade;

import com.example.appforum.entity.Role;
import com.example.appforum.entity.User;
import com.example.appforum.enums.RoleName;
import com.example.appforum.facade.dto.ResUserDto;
import com.example.appforum.security.CurrentUser;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class UserFacade {
    public ResUserDto userToUserDTO(@CurrentUser User user) {
        ResUserDto resUserDto = new ResUserDto();
        resUserDto.setId(user.getId());
        resUserDto.setEmail(user.getEmail());
        resUserDto.setFirstName(user.getFirstName());
        resUserDto.setLastName(user.getLastName());

        resUserDto.setRoleNames(user.getRoles().stream().map(role -> role.getName().toString()));
        return resUserDto;
    }
}
