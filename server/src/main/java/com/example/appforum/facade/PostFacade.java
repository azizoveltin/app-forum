package com.example.appforum.facade;

import com.example.appforum.entity.Post;
import com.example.appforum.facade.dto.ResPostDto;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Component
public class PostFacade {
    public ResPostDto postToPostDTO(Post post) {
        ResPostDto resPostDto = new ResPostDto();
        resPostDto.setEmail(post.getUser().getEmail());
        resPostDto.setId(post.getId());
        resPostDto.setBody(post.getBody());
        resPostDto.setVote(post.getVote());
        resPostDto.setVoteUser(post.getVoteUser());
        resPostDto.setTags(post.getTags());
        resPostDto.setTitle(post.getTitle());
        resPostDto.setCreatedAt(post.getCreatedAt().toLocalDateTime());
        return resPostDto;
    }
}
