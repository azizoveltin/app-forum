package com.example.appforum.facade.dto;

import lombok.Data;

@Data
public class ResCommentDto {

    private Integer id;
    private String answer;
    private String email;
}
