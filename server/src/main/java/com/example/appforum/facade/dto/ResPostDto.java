package com.example.appforum.facade.dto;

import com.example.appforum.entity.Post;
import com.example.appforum.entity.Tag;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
public class ResPostDto {

    private Integer id;
    private String title;
    private String body;
    private String email;
    private List<Tag> tags;
    private Integer vote;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm ",timezone = "Asia/Tashkent")
    private LocalDateTime createdAt ;
    private List<String> voteUser;
}
