package com.example.appforum.facade.dto;

import com.example.appforum.entity.Role;
import com.example.appforum.enums.RoleName;
import lombok.Data;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Data
public class ResUserDto {
    private UUID id;
    private String email;
    private String firstName;
    private String lastName;
    private Stream<Object> roleNames;


}

