package com.example.appforum.entity;

import com.example.appforum.entity.template.AbsIdEntity;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Comment extends AbsIdEntity {

    @Column(columnDefinition = "text", nullable = false)
    private String answer;

    @Column(nullable = false)
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    private Post post;
}
