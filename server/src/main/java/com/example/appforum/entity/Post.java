package com.example.appforum.entity;

import com.example.appforum.entity.template.AbsIdEntity;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Post  extends AbsIdEntity {
    private String title;
    @Column(columnDefinition = "text")
    private String body;
    private Integer vote;

    @Column
    @ElementCollection(targetClass = String.class)
    private List<String> voteUser;

    @ManyToOne
    private User user;

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER, mappedBy = "post", orphanRemoval = true)
    private List<Comment> comments;

    @ManyToMany
    private List<Tag> tags;

}
