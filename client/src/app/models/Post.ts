import {Comment} from './Comment';

export interface Post {
  id: number;
  title: string;
  body: string;
  vote: number;
  voteUser?: string[];
  tags: number[];
  comments?: Comment[];
  email?: string;
  createdAt: string;


}

