export interface Comment {
  id?: number;
  answer: string;
  email: string;
}
