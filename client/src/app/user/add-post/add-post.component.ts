import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Post} from "../../models/Post";
import {PostService} from "../../service/post.service";
import {NotificationService} from "../../service/notification.service";
import {Router} from "@angular/router";
import {TagService} from "../../service/tag.service";
import {Tag} from "../../models/Tag";

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  postForm: FormGroup;
  isPostCreated = false;
  createdPost: Post;
  toppings = new FormControl();

  selectedValue: []

  tags: Tag[];


  constructor(private postService: PostService,
              private notificationService: NotificationService,
              private tagService: TagService,
              private router: Router,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.postForm = this.createPostForm();
    this.tagService.getAllTags()
      .subscribe(data => {
        console.log(data);
        this.tags = data;
      });

  }


  createPostForm(): FormGroup {
    return this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      body: ['', Validators.compose([Validators.required])],
    });
  }

  submit(): void {

    this.postService.createPost({
      title: this.postForm.value.title,
      body: this.postForm.value.body,
      tags: this.selectedValue
    }).subscribe(data => {
      this.createdPost = data;
      console.log(data);

      this.notificationService.showSnackBar('Post created');
      this.isPostCreated = true;
      this.router.navigate(['/profile']);
    });

  }


}






