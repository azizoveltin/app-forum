import {Component, OnInit} from '@angular/core';
import {Post} from "../../models/Post";
import {User} from "../../models/User";
import {PostService} from "../../service/post.service";
import {UserService} from "../../service/user.service";
import {CommentService} from "../../service/comment.service";
import {NotificationService} from "../../service/notification.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {



  // isPostsLoaded = true;
  posts: Post[];
  isUserDataLoaded = false;
  user: User;
  clearValue:string = '';



  constructor(
    private postService: PostService,
    private userService: UserService,
    private commentService: CommentService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit(): void {
    this.postService.getAllPosts()
      .subscribe(data => {
        console.log(data);
        this.posts = data;
        this.getCommentsToPosts(this.posts);
        // this.isPostsLoaded = true;
      });

    this.userService.getCurrentUser()
      .subscribe(data => {
        console.log(data);
        this.user = data;
        this.isUserDataLoaded = true;

      })
  }


  getCommentsToPosts(posts: Post[]): void {
    posts.forEach(p => {
      this.commentService.getCommentsToPost(p.id)
        .subscribe(data => {
          p.comments = data
        })
    });
  }

  votePost(postId: number, postIndex: number): void {
    const post = this.posts[postIndex];
    console.log(post);

    if (!post.voteUser.includes(this.user.email)) {
      this.postService.votePost(postId, this.user.email)
        .subscribe(() => {
          post.voteUser.push(this.user.email);
          this.notificationService.showSnackBar('voted!');
        });
    } else {
      this.postService.votePost(postId, this.user.email)
        .subscribe(() => {
          const index = post.voteUser.indexOf(this.user.email, 0);
          if (index > -1) {
            post.voteUser.splice(index, 1);
          }
        });
    }
  }

  postComment(answer: string, postId: number, postIndex: number): void {
    const post = this.posts[postIndex];

    console.log(post);
    this.commentService.addToCommentToPost(postId, answer)
      .subscribe(data => {
        console.log(data);
        post.comments.push(data);
        this.clearValue = null;
      });
  }

}

