import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Post} from '../models/Post';
import {Observable} from 'rxjs';

const POST_API = 'http://localhost:/api/post/';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) {
  }


  createPost(post: { title: string; body: string,tags:number[] }): Observable<any> {
    return this.http.post(POST_API, post
    );
  }

  getAllPosts(): Observable<any> {
    return this.http.get(POST_API + 'all');
  }

  getPostForCurrentUser(): Observable<any> {
    return this.http.get(POST_API + 'user/posts');
  }

  deletePost(id: number): Observable<any> {
    return this.http.delete(POST_API + id + '/delete');
  }

  votePost(id: number, email: string): Observable<any> {
    return this.http.post(POST_API + id + '/' + email + '/vote', null);
  }

}
