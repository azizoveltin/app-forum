import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

const TAG_API = 'http://localhost:/api/tag/';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient) {
  }

  getAllTags(): Observable<any> {
    return this.http.get(TAG_API + 'all');
  }
}
